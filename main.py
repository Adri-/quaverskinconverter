import sys
import zipfile
import shutil
import os
import glob
import copy

from PIL import Image

WORKING_DIR = "./working"
OUTPUT_DIR = "./output"
INPUT_DIR = "./input"
BASE_WORKING_DIR = WORKING_DIR
CURRENT_SKIN_INI = WORKING_DIR+"/skin.ini"

blankPNG = Image.new('RGBA', (1, 1), (255, 0, 0, 0))

keys = [4,7]
keyWords = ["Name","Author"]
KkeyWords = ["HitPosition","ColumnWidth","ColumnStart","LightPosition"]
imageKeyWords = ["NoteImage","KeyImage"]

quaverResolution = 1366,768

class ConvertException(Exception):
    def __init__(self, message):
        Exception.__init__(self,message)

def tryMkdir(dir):
    try:
        if(not os.path.isdir(dir)):
            os.mkdir(dir)
    except:
        raise ConvertException("Unable to create folder \""+dir+"\"")

def getIniFileInDir(dir):
    for root, dirs, files in os.walk(".", topdown = False):
        for name in files:
            if(name.lower() == "skin.ini"):
                global CURRENT_SKIN_INI, WORKING_DIR
                CURRENT_SKIN_INI = os.path.join(root, name)
                WORKING_DIR = root


def init(inputSkin):
    WORKING_DIR = BASE_WORKING_DIR
    if(os.path.isdir(WORKING_DIR)):
        shutil.rmtree(WORKING_DIR)

    skinName = os.path.splitext(os.path.basename(inputSkin))[0]

    with zipfile.ZipFile(inputSkin,"r") as zip_ref:
        zip_ref.extractall(WORKING_DIR)

    getIniFileInDir(WORKING_DIR)

    tryMkdir(OUTPUT_DIR+"/"+skinName)

    return OUTPUT_DIR+"/"+skinName

def flipVert(image, dest, aim_width = 0, aim_height = 0, preserve_ratio = False):
    if(os.path.isfile(image)):
        if(aim_width != 0):
            w, h = getImageHeight(image)
            if(preserve_ratio):
                h = int((h/w) * aim_width)
            resizeImage(image,(aim_width,h))
        if(aim_height != 0):
            w, h = getImageHeight(image)
            if(preserve_ratio):
                w = int((w/h) * aim_height)
            resizeImage(image,(w,aim_height))
        Image.open(image).rotate(180).save(dest)
        Image.open(dest).transpose(Image.FLIP_LEFT_RIGHT).save(dest)

def flipLeft(image, dest, aim_width = 0, aim_height = 0, preserve_ratio = False):
    if(os.path.isfile(image)):
        Image.open(image).rotate(90, expand=True).save(dest)
        
        if(aim_width != 0):
            w, h = getImageHeight(dest)
            if(preserve_ratio):
                h = int((h/w) * aim_width)
            resizeImage(dest,(aim_width,h))
        if(aim_height != 0):
            w, h = getImageHeight(dest)
            if(preserve_ratio):
                w = int((w/h) * aim_height)
            resizeImage(dest,(w,aim_height))

def getImageHeight(image):
    if(os.path.isfile(image)):
        return Image.open(image).size 
    return None

def get_actual_path(parent, path):
    path.replace("\\","/")
    files = path.split('/')

    acc = ""
    for file in files:
        for f in (os.listdir(parent)):
            if f.lower() == file.lower():
                #print("Replaced "+file+" by "+f)
                file = f
        if(len(acc)):
            acc = acc + '/'
        acc = acc + file 
    return acc

def resizeImage(image,size):
    if(size[0] <= 0 or size[1] <= 0):
        return None
    img = Image.open(image)
    img = img.resize(size, Image.ANTIALIAS)
    img.save(image) 

def moveFile(filename,destination, aim_width = 0, aim_height = 0, preserve_ratio = False):
    try:
        try:
            filename = get_actual_path(WORKING_DIR, filename)
            shutil.copyfile(filename,destination)
        except:
            first_frame = filename[:-4]+"-0.png"
            shutil.copyfile(first_frame,destination)
        w, h = getImageHeight(destination)
        if(aim_width != 0):
            if(preserve_ratio):
                h = int((h/w) * aim_width)
            resizeImage(destination,(aim_width,h))
        if(aim_height != 0):
            if(preserve_ratio):
                w = int((w/h) * aim_height)
            resizeImage(destination,(w,aim_height))
        return w, h
    except:
        print("Notice: "+filename+" missing. Skipping.")
        return False

def addInImage(filename,destination, aim_width, aim_height):
    if moveFile(filename,destination):
        h, w = getImageHeight(destination)

        offset = (int(aim_height/2 - h/2),int(aim_width/2 - w/2))

        newImg = Image.new('RGBA', (aim_width,aim_height),  (0, 0, 0, 0))
        newImg.paste(Image.open(destination), offset)
        newImg.save(destination)




def do4kHitObjects(skinPathOut):
    tryMkdir(skinPathOut+"/4k")
    tryMkdir(skinPathOut+"/4k/HitObjects")

    hitObjectsFolder = skinPathOut+"/4k/HitObjects"

    sizes = {}
    
    sizes["note1"] = moveFile(WORKING_DIR+"/mania-note1.png", hitObjectsFolder+"/note-hitobject-1.png", 128, 0, True)
    sizes["note2"] = moveFile(WORKING_DIR+"/mania-note2.png", hitObjectsFolder+"/note-hitobject-2.png", 128, 0, True)
    sizes["note3"] = moveFile(WORKING_DIR+"/mania-note2.png", hitObjectsFolder+"/note-hitobject-3.png", 128, 0, True)
    sizes["note4"] = moveFile(WORKING_DIR+"/mania-note1.png", hitObjectsFolder+"/note-hitobject-4.png", 128, 0, True)
    
    sizes["note1H"] = moveFile(WORKING_DIR+"/mania-note1H.png", hitObjectsFolder+"/note-holdhitobject-1.png", 128, 0, True)
    sizes["note2H"] = moveFile(WORKING_DIR+"/mania-note2H.png", hitObjectsFolder+"/note-holdhitobject-2.png", 128, 0, True)
    sizes["note3H"] = moveFile(WORKING_DIR+"/mania-note2H.png", hitObjectsFolder+"/note-holdhitobject-3.png", 128, 0, True)
    sizes["note4H"] = moveFile(WORKING_DIR+"/mania-note1H.png", hitObjectsFolder+"/note-holdhitobject-4.png", 128, 0, True)

    
    sizes["note1L"] = moveFile(WORKING_DIR+"/mania-note1L.png", hitObjectsFolder+"/note-holdbody-1.png", 128, 0, True)
    sizes["note2L"] = moveFile(WORKING_DIR+"/mania-note2L.png", hitObjectsFolder+"/note-holdbody-2.png", 128, 0, True)
    sizes["note3L"] = moveFile(WORKING_DIR+"/mania-note2L.png", hitObjectsFolder+"/note-holdbody-3.png", 128, 0, True)
    sizes["note4L"] = moveFile(WORKING_DIR+"/mania-note1L.png", hitObjectsFolder+"/note-holdbody-4.png", 128, 0, True)

    if(not os.path.isfile(WORKING_DIR+"/mania-note1T.png")):
        flipVert(WORKING_DIR+"/mania-note1H.png", hitObjectsFolder+"/note-holdend-1.png", 128, 0, True)
        flipVert(WORKING_DIR+"/mania-note1H.png", hitObjectsFolder+"/note-holdend-4.png", 128, 0, True)
    else:
        sizes["note1T"] = moveFile(WORKING_DIR+"/mania-note1T.png", hitObjectsFolder+"/note-holdend-1.png", 128, 0, True)
        sizes["note4T"] = moveFile(WORKING_DIR+"/mania-note1T.png", hitObjectsFolder+"/note-holdend-4.png", 128, 0, True)

    if(not os.path.isfile(WORKING_DIR+"/mania-note2T.png")):
        flipVert(WORKING_DIR+"/mania-note2H.png", hitObjectsFolder+"/note-holdend-2.png", 128, 0, True)
        flipVert(WORKING_DIR+"/mania-note2H.png", hitObjectsFolder+"/note-holdend-3.png", 128, 0, True)
    else:
        sizes["note2T"] = moveFile(WORKING_DIR+"/mania-note2T.png", hitObjectsFolder+"/note-holdend-2.png", 128, 0, True)
        sizes["note3T"] = moveFile(WORKING_DIR+"/mania-note2T.png", hitObjectsFolder+"/note-holdend-3.png", 128, 0, True)

    return sizes

def do7kHitObjects(skinPathOut):
    tryMkdir(skinPathOut+"/7k")
    tryMkdir(skinPathOut+"/7k/HitObjects")

    hitObjectsFolder = skinPathOut+"/7k/HitObjects"

    sizes = {}

    sizes["note1"] = moveFile(WORKING_DIR+"/mania-note1.png", hitObjectsFolder+"/note-hitobject-1.png", 128, 0, True)
    sizes["note2"] = moveFile(WORKING_DIR+"/mania-note2.png", hitObjectsFolder+"/note-hitobject-2.png", 128, 0, True)
    sizes["note3"] = moveFile(WORKING_DIR+"/mania-note1.png", hitObjectsFolder+"/note-hitobject-3.png", 128, 0, True)
    sizes["note4"] = moveFile(WORKING_DIR+"/mania-noteS.png", hitObjectsFolder+"/note-hitobject-4.png", 128, 0, True)
    sizes["note5"] = moveFile(WORKING_DIR+"/mania-note1.png", hitObjectsFolder+"/note-hitobject-5.png", 128, 0, True)
    sizes["note6"] = moveFile(WORKING_DIR+"/mania-note2.png", hitObjectsFolder+"/note-hitobject-6.png", 128, 0, True)
    sizes["note7"] = moveFile(WORKING_DIR+"/mania-note1.png", hitObjectsFolder+"/note-hitobject-7.png", 128, 0, True)
    
    sizes["note1H"] = moveFile(WORKING_DIR+"/mania-note1H.png", hitObjectsFolder+"/note-holdhitobject-1.png", 128, 0, True)
    sizes["note2H"] = moveFile(WORKING_DIR+"/mania-note2H.png", hitObjectsFolder+"/note-holdhitobject-2.png", 128, 0, True)
    sizes["note3H"] = moveFile(WORKING_DIR+"/mania-note1H.png", hitObjectsFolder+"/note-holdhitobject-3.png", 128, 0, True)
    sizes["note4H"] = moveFile(WORKING_DIR+"/mania-noteSH.png", hitObjectsFolder+"/note-holdhitobject-4.png", 128, 0, True)
    sizes["note5H"] = moveFile(WORKING_DIR+"/mania-note1H.png", hitObjectsFolder+"/note-holdhitobject-5.png", 128, 0, True)
    sizes["note6H"] = moveFile(WORKING_DIR+"/mania-note2H.png", hitObjectsFolder+"/note-holdhitobject-6.png", 128, 0, True)
    sizes["note7H"] = moveFile(WORKING_DIR+"/mania-note1H.png", hitObjectsFolder+"/note-holdhitobject-7.png", 128, 0, True)

    
    sizes["note1L"] = moveFile(WORKING_DIR+"/mania-note1L.png", hitObjectsFolder+"/note-holdbody-1.png", 128, 0, True)
    sizes["note2L"] = moveFile(WORKING_DIR+"/mania-note2L.png", hitObjectsFolder+"/note-holdbody-2.png", 128, 0, True)
    sizes["note3L"] = moveFile(WORKING_DIR+"/mania-note1L.png", hitObjectsFolder+"/note-holdbody-3.png", 128, 0, True)
    sizes["note4L"] = moveFile(WORKING_DIR+"/mania-noteSL.png", hitObjectsFolder+"/note-holdbody-4.png", 128, 0, True)
    sizes["note5L"] = moveFile(WORKING_DIR+"/mania-note1L.png", hitObjectsFolder+"/note-holdbody-5.png", 128, 0, True)
    sizes["note6L"] = moveFile(WORKING_DIR+"/mania-note2L.png", hitObjectsFolder+"/note-holdbody-6.png", 128, 0, True)
    sizes["note7L"] = moveFile(WORKING_DIR+"/mania-note1L.png", hitObjectsFolder+"/note-holdbody-7.png", 128, 0, True)

    if(not os.path.isfile(WORKING_DIR+"/mania-note1T.png")):
        flipVert(WORKING_DIR+"/mania-note1H.png", hitObjectsFolder+"/note-holdend-1.png", 128, 0, True)
        flipVert(WORKING_DIR+"/mania-note1H.png", hitObjectsFolder+"/note-holdend-3.png", 128, 0, True)
        flipVert(WORKING_DIR+"/mania-note1H.png", hitObjectsFolder+"/note-holdend-5.png", 128, 0, True)
        flipVert(WORKING_DIR+"/mania-note1H.png", hitObjectsFolder+"/note-holdend-7.png", 128, 0, True)
    else:
        sizes["note1T"] = moveFile(WORKING_DIR+"/mania-note1T.png", hitObjectsFolder+"/note-holdend-1.png", 128, 0, True)
        sizes["note3T"] = moveFile(WORKING_DIR+"/mania-note1T.png", hitObjectsFolder+"/note-holdend-3.png", 128, 0, True)
        sizes["note5T"] = moveFile(WORKING_DIR+"/mania-note1T.png", hitObjectsFolder+"/note-holdend-5.png", 128, 0, True)
        sizes["note7T"] = moveFile(WORKING_DIR+"/mania-note1T.png", hitObjectsFolder+"/note-holdend-7.png", 128, 0, True)

    if(not os.path.isfile(WORKING_DIR+"/mania-note2T.png")):
        flipVert(WORKING_DIR+"/mania-note2H.png", hitObjectsFolder+"/note-holdend-2.png", 128, 0, True)
        flipVert(WORKING_DIR+"/mania-note2H.png", hitObjectsFolder+"/note-holdend-6.png", 128, 0, True)
    else:
        sizes["note2T"] = moveFile(WORKING_DIR+"/mania-note2T.png", hitObjectsFolder+"/note-holdend-2.png", 128, 0, True)
        sizes["note6T"] = moveFile(WORKING_DIR+"/mania-note2T.png", hitObjectsFolder+"/note-holdend-6.png", 128, 0, True)

    if(not os.path.isfile(WORKING_DIR+"/mania-noteST.png")):
        flipVert(WORKING_DIR+"/mania-noteSH.png", hitObjectsFolder+"/note-holdend-4.png", 128, 0, True)
    else:
        sizes["note7T"] = moveFile(WORKING_DIR+"/mania-noteST.png", hitObjectsFolder+"/note-holdend-4.png", 128, 0, True)

    return sizes


def do4kReceptors(skinPathOut):
    tryMkdir(skinPathOut+"/4k")
    tryMkdir(skinPathOut+"/4k/Receptors")

    receptorsFolder = skinPathOut+"/4k/Receptors"

    sizes = {}

    sizes["key1"] = moveFile(WORKING_DIR+"/mania-key1.png", receptorsFolder+"/receptor-up-1.png",100)
    sizes["key2"] = moveFile(WORKING_DIR+"/mania-key2.png", receptorsFolder+"/receptor-up-2.png",100)
    sizes["key3"] = moveFile(WORKING_DIR+"/mania-key2.png", receptorsFolder+"/receptor-up-3.png",100)
    sizes["key4"] = moveFile(WORKING_DIR+"/mania-key1.png", receptorsFolder+"/receptor-up-4.png",100)

    sizes["key1D"] = moveFile(WORKING_DIR+"/mania-key1D.png", receptorsFolder+"/receptor-down-1.png",100)
    sizes["key2D"] = moveFile(WORKING_DIR+"/mania-key2D.png", receptorsFolder+"/receptor-down-2.png",100)
    sizes["key3D"] = moveFile(WORKING_DIR+"/mania-key2D.png", receptorsFolder+"/receptor-down-3.png",100)
    sizes["key4D"] = moveFile(WORKING_DIR+"/mania-key1D.png", receptorsFolder+"/receptor-down-4.png",100)

    return sizes

def do7kReceptors(skinPathOut):
    tryMkdir(skinPathOut+"/7k")
    tryMkdir(skinPathOut+"/7k/Receptors")

    receptorsFolder = skinPathOut+"/7k/Receptors"

    sizes = {}

    sizes["key1"] = moveFile(WORKING_DIR+"/mania-key1.png", receptorsFolder+"/receptor-up-1.png",100)
    sizes["key2"] = moveFile(WORKING_DIR+"/mania-key2.png", receptorsFolder+"/receptor-up-2.png",100)
    sizes["key3"] = moveFile(WORKING_DIR+"/mania-key1.png", receptorsFolder+"/receptor-up-3.png",100)
    sizes["key4"] = moveFile(WORKING_DIR+"/mania-keyS.png", receptorsFolder+"/receptor-up-4.png",100)
    sizes["key5"] = moveFile(WORKING_DIR+"/mania-key1.png", receptorsFolder+"/receptor-up-5.png",100)
    sizes["key6"] = moveFile(WORKING_DIR+"/mania-key2.png", receptorsFolder+"/receptor-up-6.png",100)
    sizes["key7"] = moveFile(WORKING_DIR+"/mania-key1.png", receptorsFolder+"/receptor-up-7.png",100)

    sizes["key1D"] = moveFile(WORKING_DIR+"/mania-key1D.png", receptorsFolder+"/receptor-down-1.png",100)
    sizes["key2D"] = moveFile(WORKING_DIR+"/mania-key2D.png", receptorsFolder+"/receptor-down-2.png",100)
    sizes["key3D"] = moveFile(WORKING_DIR+"/mania-key1D.png", receptorsFolder+"/receptor-down-3.png",100)
    sizes["key4D"] = moveFile(WORKING_DIR+"/mania-keySD.png", receptorsFolder+"/receptor-down-4.png",100)
    sizes["key5D"] = moveFile(WORKING_DIR+"/mania-key1D.png", receptorsFolder+"/receptor-down-5.png",100)
    sizes["key6D"] = moveFile(WORKING_DIR+"/mania-key2D.png", receptorsFolder+"/receptor-down-6.png",100)
    sizes["key7D"] = moveFile(WORKING_DIR+"/mania-key1D.png", receptorsFolder+"/receptor-down-7.png",100)

    return sizes

def doLighting(skinPathOut, key):
    tryMkdir(skinPathOut+"/"+str(key)+"k")
    tryMkdir(skinPathOut+"/"+str(key)+"k/Lighting")

    lightingFolder = skinPathOut+"/"+str(key)+"k/Lighting"

    sizes = {}

    sizes["stagelight"] = moveFile(WORKING_DIR+"/mania-stage-light.png", lightingFolder+"/column-lighting.png",100,0,True)
    if not sizes["stagelight"]:
        try:
            blankPNG.save(lightingFolder+"/column-lighting.png")
            sizes["stagelight"] = 1
        except:
            print("IMPORTANT: Could not create ColumnLighting.")

    sizes["lightingL"] = moveFile(WORKING_DIR+"/lightingL.png", lightingFolder+"/holdlighting.png",100,0,True)
    sizes["lightingN"] = moveFile(WORKING_DIR+"/lightingN.png", lightingFolder+"/hitlighting.png",100,0,True)

    return sizes

def doStage(skinPathOut, key):
    tryMkdir(skinPathOut+"/"+str(key)+"k")
    tryMkdir(skinPathOut+"/"+str(key)+"k/Stage")

    stageFolder = skinPathOut+"/"+str(key)+"k/Stage"

    sizes = {}

    sizes["stageright"] = moveFile(WORKING_DIR+"/mania-stage-right.png", stageFolder+"/stage-right-border.png")
    sizes["stageleft"] = moveFile(WORKING_DIR+"/mania-stage-left.png", stageFolder+"/stage-left-border.png")

    return sizes


def do4k(skinPathOut):
    sizes = {}
    sizes["HitObject"] = do4kHitObjects(skinPathOut)
    sizes["Receptors"] = do4kReceptors(skinPathOut)
    sizes["Lighting"] = doLighting(skinPathOut,4)
    sizes["Stage"] = doStage(skinPathOut,4)
    return sizes

def do7k(skinPathOut):
    sizes = {}
    sizes["HitObject"] = do7kHitObjects(skinPathOut)
    sizes["Receptors"] = do7kReceptors(skinPathOut)
    sizes["Lighting"] = doLighting(skinPathOut,7)
    sizes["Stage"] = doStage(skinPathOut,7)
    return sizes

def doJudgement(skinPathOut):
    tryMkdir(skinPathOut+"/Judgements")

    judgementsFolder = skinPathOut+"/Judgements"

    judges = [
        [WORKING_DIR+"/mania-hit0.png",judgementsFolder+"/judge-miss.png"],
        [WORKING_DIR+"/mania-hit50.png",judgementsFolder+"/judge-okay.png"],
        [WORKING_DIR+"/mania-hit100.png",judgementsFolder+"/judge-good.png"],
        [WORKING_DIR+"/mania-hit200.png",judgementsFolder+"/judge-great.png"],
        [WORKING_DIR+"/mania-hit300.png",judgementsFolder+"/judge-perf.png"],
        [WORKING_DIR+"/mania-hit300g.png",judgementsFolder+"/judge-marv.png"]
    ]

    w = 0
    h = 0
    for judge in judges:
        try:
            nw, nh = getImageHeight(judge[0])

            if(nw > w):
                w = nw
            if(nh > h):
                h = nh
        except:
            a = 0

    if(w>h):
        h = w

    for judge in judges:
        addInImage(judge[0], judge[1],h,h)

def doGrades(skinPathOut):
    tryMkdir(skinPathOut+"/Grades")

    gradesFolder = skinPathOut+"/Grades"
    
    moveFile(WORKING_DIR+"/ranking-D.png", gradesFolder+"/grade-small-d.png",100,0,True)
    moveFile(WORKING_DIR+"/ranking-C.png", gradesFolder+"/grade-small-c.png",100,0,True)
    moveFile(WORKING_DIR+"/ranking-B.png", gradesFolder+"/grade-small-b.png",100,0,True)
    moveFile(WORKING_DIR+"/ranking-A.png", gradesFolder+"/grade-small-a.png",100,0,True)
    moveFile(WORKING_DIR+"/ranking-S.png", gradesFolder+"/grade-small-s.png",100,0,True)
    moveFile(WORKING_DIR+"/ranking-X.png", gradesFolder+"/grade-small-ss.png",100,0,True)

def doPause(skinPathOut):
    tryMkdir(skinPathOut+"/Pause")

    pauseFolder = skinPathOut+"/Pause"
    
    moveFile(WORKING_DIR+"/pause-back.png", pauseFolder+"/pause-back.png")
    moveFile(WORKING_DIR+"/pause-continue.png", pauseFolder+"/pause-continue.png")
    moveFile(WORKING_DIR+"/pause-retry.png", pauseFolder+"/pause-retry.png")
    moveFile(WORKING_DIR+"/pause-overlay.png", pauseFolder+"/pause-background.png")

def doHealth(skinPathOut):
    tryMkdir(skinPathOut+"/Health")

    healthFolder = skinPathOut+"/Health"

    flipLeft(WORKING_DIR+"/scorebar-bg.png", healthFolder+"/health-background.png",0,600,True)
    flipLeft(WORKING_DIR+"/scorebar-colour.png", healthFolder+"/health-foreground.png",0,600,True)

def doCursor(skinPathOut):
    tryMkdir(skinPathOut+"/Cursor")

    cursorFolder = skinPathOut+"/Cursor"

    moveFile(WORKING_DIR+"/cursor.png", cursorFolder+"/main-cursor.png")

def doScoreboard(skinPathOut):
    tryMkdir(skinPathOut+"/Scoreboard")

def doSFX(skinPathOut):
    tryMkdir(skinPathOut+"/SFX")

    sfxFolder = skinPathOut+"/SFX"
    
    moveFile(WORKING_DIR+"/normal-hitclap.wav", sfxFolder+"/sound-hitclap.wav")
    moveFile(WORKING_DIR+"/normal-hitnormal.wav", sfxFolder+"/sound-hit.wav")
    moveFile(WORKING_DIR+"/normal-hitwhistle.wav", sfxFolder+"/sound-hitwhistle.wav")
    moveFile(WORKING_DIR+"/normal-hitfinish.wav", sfxFolder+"/sound-hitfinish.wav")
    moveFile(WORKING_DIR+"/failsound.wav", sfxFolder+"/sound-failure.wav")
    moveFile(WORKING_DIR+"/combobreak.wav", sfxFolder+"/sound-combobreak.wav")


def doRefImage(skinPathOut, source):
    for k in keys:
        ks = str(k)
        for line in source[ks+"k"]["IniReferences"]:
            keyWord, filePath = line.split(":")

            filePath = filePath.strip()

            if(keyWord[-1].isdigit()):
                fileType = keyWord[-1:]
            else:
                fileType = keyWord[-2:]

            filePathFull = WORKING_DIR+"/"+filePath+".png"
            if(keyWord.startswith("NoteImage")):
                # Not supporting >9k
                if(len(fileType) == 1):
                    key = str(int(fileType) + 1)
                    moveFile(filePathFull, skinPathOut+"/"+ks+"k/HitObjects/note-hitobject-"+key+".png", 128, 0, True)
                else:
                    key = str(int(fileType[0]) + 1)
                    if(fileType[1] == 'H'):
                        moveFile(filePathFull, skinPathOut+"/"+ks+"k/HitObjects/note-holdhitobject-"+key+".png", 128, 0, True)
                        flipVert(filePathFull, skinPathOut+"/"+ks+"k/HitObjects/note-holdend-"+key+".png", 128, 0, True)
                    if(fileType[1] == 'L'):
                        moveFile(filePathFull, skinPathOut+"/"+ks+"k/HitObjects/note-holdbody-"+key+".png", 128, 0, True)
                    if(fileType[1] == 'T'):
                        flipVert(filePathFull, skinPathOut+"/"+ks+"k/HitObjects/note-holdend-"+key+".png", 128, 0, True)
            
            if(keyWord.startswith("KeyImage")):
                # Not supporting >9k
                if(len(fileType) == 1):
                    key = str(int(fileType) + 1)
                    moveFile(filePathFull, skinPathOut+"/"+ks+"k/Receptors/receptor-up-"+key+".png",100)
                else:
                    key = str(int(fileType[0]) + 1)
                    moveFile(filePathFull, skinPathOut+"/"+ks+"k/Receptors/receptor-down-"+key+".png",100)

def getIfExists(keyWord, dicti, default = 0):
    if(keyWord in dicti):
        return dicti[str(keyWord)]
    else:
        return default

def playFieldYOsuToQuaver(value):
    return int(((485 - int(value))/485)*quaverResolution[1])
    
def playFieldXOsuToQuaver(value):
    return int(((int(value)-425)/850)*(quaverResolution[0]/2))

def doIni(skinPathOut, imageSizes):
    global CURRENT_SKIN_INI
    data, source = getIni(imageSizes)

    doRefImage(skinPathOut, source)

    with open(skinPathOut+"/skin.ini", 'w+') as f:
        f.write("[General]\n")
        f.write("Name = "+data["Name"]+"\n")
        f.write("Author = "+data["Author"]+"\n")

        for k in keys:
            ks = str(k)

            src = source[ks+"k"]
 
            lightPosition = playFieldYOsuToQuaver(getIfExists("LightPosition",src,413))
            hitPosition = playFieldYOsuToQuaver(getIfExists("HitPosition",src,402))
            columnStart = playFieldXOsuToQuaver(getIfExists("ColumnStart",src,136))

            try:
                imgKeyWidth, imgKeyHeight = getImageHeight(skinPathOut+"/"+ks+"k/Receptors/receptor-down-1.png")
            except:
                imgKeyHeight = 0

            data[ks+"k"]["ColumnAlignment"] = columnStart
            data[ks+"k"]["HitPosOffsetY"] = int(imgKeyHeight - hitPosition)
            data[ks+"k"]["ColumnLightingOffsetY"] = lightPosition


            f.write("\n["+str(k)+"K]\n")
            for key in data[str(k)+"k"].keys():
                f.write(key+" = "+str(data[str(k)+"k"][key])+"\n")

def getIni(imageSizes):
    keyMode = 0
    data = {}
    source = {}

    for keyWord in keyWords:
        data[""+keyWord] = "None"

    baseData = {}
    baseData["4"] = {
        'AccuracyDisplayPosX': -10,
        'AccuracyDisplayPosY': 5,
        'AccuracyDisplayScale': 18,
        'BattleRoyaleAlertPosX': 0,
        'BattleRoyaleAlertPosY': -150,
        'BattleRoyaleAlertScale': 110,
        'BattleRoyaleEliminatedPosX': 0,
        'BattleRoyaleEliminatedPosY': -115,
        'BgMaskAlpha': 1,
        'BgMaskPadding': 0,
        'ColorObjectsBySnapDistance': "False",
        'ColumnAlignment': 0,
        'ColumnColor1': "255,255,255",
        'ColumnColor2': "255,255,255",
        'ColumnColor3': "255,255,255",
        'ColumnColor4': "255,255,255",
        'ColumnLightingOffsetY': 0,
        'ColumnLightingScale': 1.0,
        'ColumnSize': 90,
        'ComboDisplayScale': 45,
        'ComboPosX': 0,
        'ComboPosY': -40,
        'DeadNoteColor': "200,200,200",
        'DrawLongNoteEnd': "True",
        'FlipNoteEndImagesOnUpscroll': "True",
        'FlipNoteImagesOnUpscroll': "True",
        'HealthBarKeysAlignment': "RightStage",
        'HealthBarType': "Vertical",
        'HealthBarPosOffsetX': 0,
        'HealthBarPosOffsetY': 0,
        'HealthBarScale': 100,
        'HitErrorChevronSize': 8,
        'HitErrorHeight': 10,
        'HitErrorPosX': 0,
        'HitErrorPosY': 45,
        'HitLightingFps': 60,
        'HitLightingHeight': 50,
        'HitLightingWidth': 110,
        'HitLightingX': 0,
        'HitLightingY': 28,
        'HitPosOffsetY': 90,
        'HoldLightingFps': 60,
        'JudgementBurstPosY': 108,
        'JudgementCounterAlpha': 1,
        'JudgementCounterFontColor': "255,255,255",
        'JudgementCounterSize': 40,
        'JudgementHitBurstScale': 100,
        'KpsDisplayPosX': -10,
        'KpsDisplayPosY': 10,
        'KpsDisplayScale': 18,
        'NotePadding': 0,
        'RatingDisplayPosX': 10,
        'RatingDisplayPosY': 8,
        'RatingDisplayScale': 18,
        'ReceptorPosOffsetY': 5,
        'ReceptorsOverHitObjects': "False",
        'ScoreDisplayPosX': 10,
        'ScoreDisplayPosY': 5,
        'ScoreDisplayScale': 18,
        'SongTimeProgressActiveColor': "22,174,247",
        'SongTimeProgressInactiveColor': "136,136,136",
        'SongTimeProgressScale': 45,
        'StageReceptorPadding': 0,
        'TimingBarPixelSize': 2,
        'TimingLineColor': "255,255,255",
        'UseAndRotateHitObjectSheet': "False"
    }

    baseData["7"] = baseData["4"]

    baseData["7"]["ColumnColor5"] = "255,255,255"
    baseData["7"]["ColumnColor6"] = "255,255,255"
    baseData["7"]["ColumnColor7"] = "255,255,255"

    keys = [4,7]

    for k in keys:
        data[str(k)+"k"] = copy.deepcopy(baseData[str(k)])
        source[str(k)+"k"] = {}

    for line in open(CURRENT_SKIN_INI,"r").readlines():

        if( int(keyMode) != 0 and int(keyMode) in keys):
            for keyWord in imageKeyWords:
                if(line.startswith(keyWord)):
                    source[keyMode+"k"]["IniReferences"].append(line)


            for keyWord in KkeyWords:
                if(line.startswith(keyWord)):
                    source[keyMode+"k"][""+keyWord] = line[(len(keyWord)+1):].strip()
                    
            if "IniReferences" not in source[keyMode+"k"].keys():
                source[keyMode+"k"]["IniReferences"] = []
        
        
        for keyWord in keyWords:
            if(line.startswith(keyWord) or line.startswith(keyWord)):
                data[""+keyWord] = line[(len(line.split(":")[0])+1):].strip()
            
    
        if(line.startswith("Keys ") or line.startswith("Keys:")):
            keyMode = line[(len(line.split(":")[0])+1):].strip()

        
    for k in keys:
        ks = str(k)
        if(ks+"k" in source and "ColumnWidth" in source[ks+"k"]):
            data[ks+"k"]["ColumnSize"] = ColumnWidthToColumnSize(int(source[ks+"k"]["ColumnWidth"].split(",")[0]))
        if(ks+"k" in source and "ColumnSpacing" in source[ks+"k"]):
            data[ks+"k"]["NotePadding"] = ColumnSpacingToNotePadding(int(source[ks+"k"]["ColumnSpacing"].split(",")[0]))
        if(ks+"k" in source and imageSizes[ks+"k"]["Receptors"]["key1"] != False and "HitPosition" in source[ks+"k"]):
            data[ks+"k"]["HitPosOffsetY"] = GetHitPosOffsetY(data[ks+"k"]["ColumnSize"], imageSizes[ks+"k"]["Receptors"]["key1"][1],0,imageSizes[ks+"k"]["Receptors"]["key1"][0], int(source[ks+"k"]["HitPosition"]))
        if(ks+"k" in source and "ComboPosition" in source[ks+"k"]):
            data[ks+"k"]["ComboPosY"] = ComboPositionToComboPosY(int(source[ks+"k"]["ComboPosition"]))
        if(ks+"k" in source and "ScorePosition" in source[ks+"k"]):
            data[ks+"k"]["JudgementBurstPosY"] = ScorePositionToJudgementBurstPosY(int(source[ks+"k"]["ScorePosition"]))
        if(ks+"k" in source and imageSizes[ks+"k"]["Lighting"]["lightingN"] != False):
            data[ks+"k"]["HitLightingWidth"] = imageSizes[ks+"k"]["Lighting"]["lightingN"][0]
            data[ks+"k"]["HitLightingHeight"] = imageSizes[ks+"k"]["Lighting"]["lightingN"][1]



    return data, source

def ColumnWidthToColumnSize(ColumnWidth):
    return int(ColumnWidth*768/480.0)


def ColumnSpacingToNotePadding(ColumnSpacing):
    return int(ColumnSpacing*768/480.0)


def GetReceptorPosOffsetY(ColumnSize, ReceptorHeight, ReceptorUpperBlankSpaceHeight, ReceptorWidth):
    return (ReceptorHeight-ReceptorUpperBlankSpaceHeight) - int((ReceptorHeight-ReceptorUpperBlankSpaceHeight) * ColumnSize/ReceptorWidth)

def GetHitPosOffsetY(ColumnSize, ReceptorHeight, ReceptorUpperBlankSpaceHeight, ReceptorWidth, Hitposition):
    return (ReceptorHeight-ReceptorUpperBlankSpaceHeight) - (768-int(Hitposition*768/480.0)) + int(ReceptorUpperBlankSpaceHeight*ColumnSize)

def ComboPositionToComboPosY(ComboPosition):
    return int((ComboPosition*768/480)-384.0)

def ScorePositionToJudgementBurstPosY(ScorePosition):
    return int((ScorePosition*768/480)-384.0)

def GetHitLightingY(ColumnSize, ReceptorHeight, ReceptorUpperBlankSpaceHeight, ReceptorWidth, Hitposition):
    return int(ReceptorUpperBlankSpaceHeight*ColumnSize/ReceptorWidth)-(int(ReceptorHeight*ColumnSize/ReceptorWidth)/2)-(768-int(Hitposition*768/480.0))+(ReceptorHeight-ReceptorUpperBlankSpaceHeight)


def main(args):
    print("Starting")
    tryMkdir(OUTPUT_DIR)
    tryMkdir(INPUT_DIR)

    for file in (os.listdir(INPUT_DIR)):
        skinFile = INPUT_DIR+"/"+file
        if (os.path.splitext(skinFile)[1] == ".osk"):
            print("-------- "+file+" --------")

            imageSizes = {}

            try:
                skinPathOut = init(skinFile)

                imageSizes["4k"] = do4k(skinPathOut)
                imageSizes["7k"] = do7k(skinPathOut)
                doJudgement(skinPathOut)
                doGrades(skinPathOut)
                doPause(skinPathOut)
                doHealth(skinPathOut)
                doCursor(skinPathOut)
                doScoreboard(skinPathOut)
                doSFX(skinPathOut)
                
                doIni(skinPathOut, imageSizes)

            except ConvertException as e:
                print(e)
                sys.exit(1)

            print(file+" has been converted.")
        shutil.rmtree(BASE_WORKING_DIR)

        
    print("All skins have been converted.")
    print("\n")
    print("WARNING: The skin might still need to be worked on, some things cannot be automatically converted.")
    print("IMPORTANT: If you had percys with very high images, these might crash the game. Check your LNbodies.")
    print("\n")
    input("When you have read that, press Enter")

    


##########

main(sys.argv)