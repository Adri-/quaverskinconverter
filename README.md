## Downloads

You can download the standalone at https://rhythmgamers.net/QBC/

## How to use

1. Put the latest executable in a folder
2. Create a new folder "input"
3. Put one or more skins as .osk in it
4. Run the executable
5. Skins will be generated in a new "output" folder

still very much a WiP but can provide a basic skin and actually handles the hitposition without you needing to wreck your head on the system

## Changelog

Latest version: alpha 0.6

- added more default config values

Known issues:
- The health bar foreground can be wrongly placed (can be fixed manually though image editing)
- The cursor is only anchored on the top left in quaver when it's the middle in osu. If you have a circle shaped cursor it will feel very weird until it is handled in-game.

Features recap:
- Import and resize most images to get the same look and feel than osu!mania (gameplay)
- Import as many audio elements as possible
- Find the correct HitPosition depending on receptors and notes size to be the same as osu!mania's hitposition
- Correctly import judgments and UI elements such as Cursor and fail-menu / pause menu buttons



## Compiling

You need python and Pillow

1. Clone this repo
2. Add your skin (.osk) in the `input` folder
3. Run the program
4. You will find the converted skin in `output`